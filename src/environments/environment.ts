// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD9iGZ8J8KoOB6eBcXSTmnZjhFGemeHcTk",
    authDomain: "amplifynature-8be2d.firebaseapp.com",
    databaseURL: "https://amplifynature-8be2d.firebaseio.com",
    projectId: "amplifynature-8be2d",
    storageBucket: "amplifynature-8be2d.appspot.com",
    messagingSenderId: "459285736048"

  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
