import { Component, OnInit, ViewChild } from '@angular/core';
import { PhraseService } from '../../shared/services/phrase.service';
import { EnjeuService } from '../../shared/services/enjeu.service';
import { CsvPhrase } from '../../shared/models/csv-phrase.model';
import { Enjeu } from '../../shared/models/enjeu.model';
import { Phrase } from '../../shared/models/phrase.model';
import { NgbPanelChangeEvent, NgbAccordion } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';


@Component({
  selector: 'app-phrase',
  templateUrl: './phrase.component.html',
  styleUrls: ['./phrase.component.scss']
})
export class PhraseComponent implements OnInit {

	@ViewChild('acc') accordion: NgbAccordion;

	csvPhrases:CsvPhrase[] = [];
	prevCsv:any;
	curCsv:any;
	curPhrase:any;
	prevPhrase:any;

	perf:number = 0;

	loading:Boolean = false;
	enjeux:Enjeu[];

	firstLoading:boolean = true;
  constructor(
  	public phraseService:PhraseService,
  	public enjeuService:EnjeuService,

  	) { }



  ngOnInit() {
  	let self = this;
  	self.loading = true;
  	this.phraseService.getWithPhrases().once("value", function(data){

  		// let csvPhrase = data.map(a => ({ nom: a.key, ...a.payload.val() }));
      let csvPhrase =  [];
      data.forEach(function(child){
        let item:CsvPhrase = child.val();
        item.nom = child.key;
        csvPhrase.push(item);
      })
      
  		self.csvPhrases = csvPhrase.map((a:CsvPhrase) => new CsvPhrase().deserialize(a));
  		let iter1 = 1;
  		for(let i in self.csvPhrases){

  			if(!self.csvPhrases[i].valid || iter1 == self.csvPhrases.length){

  				self.csvPhrases[i].active = true;
  				self.curCsv = i;
  				let iter = 1;
          console.log("le csv phrase %o", self.csvPhrases[i])
          console.log("les phases dans le csv %o", self.csvPhrases[i].phrases)
  				for(let j in self.csvPhrases[i].phrases){
            console.log("check valid %o", self.csvPhrases[i].phrases[j].valid);
  					if(!self.csvPhrases[i].phrases[j].valid || iter == self.csvPhrases[i].phrases.length){
  						self.curPhrase = j;


  						break;
  					}
  					iter+=1;
  				}
  				break;
  			}
  			iter1+=1;
  		}

      self.enjeuService.getAll().subscribe(function(data){
                let hidden = document.getElementsByClassName('bg-secondary') ;
                for (let i = 0; i < hidden.length; ++i) {
                  (hidden[i] as HTMLElement).style.display = "none";
                }
                self.loading = false;
                let enjeux = data.map(a => ({ key: a.key, ...a.payload.val() }));
                self.enjeux = enjeux.map((a:Enjeu) => new Enjeu().deserialize(a));
                self.getPerf();
      })


 
  		console.log('CSV PHRASES %o', self.csvPhrases);
  	});

  	

  	
  }

  ngAfterViewInit(){
  	let self = this;
    // alert('INIT');
  	  
      // console.log((document.getElementsByClassName('bg-secondary')[0] as HTMLElement));
  		let iter1 = 1;
  		for(let i in self.csvPhrases){
  			if(!self.csvPhrases[i].valid  || iter1 == self.csvPhrases.length){

  				(document.getElementById(self.csvPhrases[i].nom+'-header').children[0].children[0] as HTMLElement).style.color = '#ffc107';
  				break;
  			}
  			iter1 +=1;
  		}
  	// this.getPerf();
  }


  next(){
  	window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
  	this.loading = true;
  	let self = this;
    self.csvPhrases[self.curCsv].phrases[self.curPhrase].valid = true;
  	self.csvPhrases[self.curCsv].phrases[self.curPhrase].compteur_perf = this.perf.toString();
  	this.phraseService.update(this.csvPhrases[this.curCsv],self.csvPhrases[self.curCsv].phrases[self.curPhrase], self.curPhrase)
    // .then(function(){
  		self.loading = false;
  		

	  	for(let i in self.csvPhrases){
	  			if(!self.csvPhrases[i].valid){
	  				self.csvPhrases[i].active = true;
	  				(document.getElementById(self.csvPhrases[i].nom+'-header').children[0].children[0] as HTMLElement).style.color = '#ffc107';
	  				self.curCsv = i;
	  				let iter = 1;
	  				for(let j in self.csvPhrases[i].phrases){
	  					if(!self.csvPhrases[i].phrases[j].valid){
	  						self.curPhrase = j;
                

	  						break;
	  					}
              console.log(iter);
                console.log(iter == self.csvPhrases[i].phrases.length);
              if(iter == self.csvPhrases[i].phrases.length){
                  console.log('valid');
                  self.csvPhrases[i].valid = true;
                  self.phraseService.validate(self.csvPhrases[i])

                  self.next();
                  break;
              }
	  					iter+=1;
	  				}
            break;
	  				
	  			}else{
	  				(document.getElementById(self.csvPhrases[i].nom+'-header').children[0].children[0] as HTMLElement).style.color = '#b0bec5';
	  			}
	  		}
	  	self.getPerf();
  	// })
	  	
  }

  previous(){
  	window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
  	this.loading = true;
  	let prevPhrase;
  	for(let i in this.csvPhrases[this.curCsv].phrases){
  		if(this.curPhrase == i){
  			this.curPhrase = prevPhrase;
  		}
  		prevPhrase = i;
  	}
  	this.loading = false;
  	
  	this.getPerf();
  }

  setEnjeu(enjeu){
  	
  	console.log("set enjeu %o",this.csvPhrases[this.curCsv].phrases[this.curPhrase] );
    if(enjeu != 'none'){
      this.csvPhrases[this.curCsv].phrases[this.curPhrase].enjeu = enjeu.def.replace(/#/g, '');
      this.perf = this.computePerf(this.csvPhrases[this.curCsv].phrases[this.curPhrase], enjeu);
    }else{
      this.csvPhrases[this.curCsv].phrases[this.curPhrase].enjeu = enjeu;
      this.perf = 0;
    }
    // console.log("AFTER setTING enjeu %o",this.csvPhrases[this.curCsv].phrases[this.curPhrase] );

  	
  	// console.log("perf %o", this.perf);
  }

  setSatisfaction(i){
  	this.csvPhrases[this.curCsv].phrases[this.curPhrase].satisfaction = i;
    this.next();
  }

  setPhrase(i, j){

  	this.curCsv = i;
  	this.curPhrase = j;
  	for( let k in this.csvPhrases){
  		if(k == i){
  			(document.getElementById(this.csvPhrases[k].nom+'-header').children[0].children[0] as HTMLElement).style.color = '#ffc107';
  		}else{
  			(document.getElementById(this.csvPhrases[k].nom+'-header').children[0].children[0] as HTMLElement).style.color = '#b0bec5';
  		}
  	}
  	this.getPerf();
  }

  getPerf(){
    // console.log("getting perf");
   
  	if(this.csvPhrases[this.curCsv].phrases[this.curPhrase].enjeu == "" ){
  		this.getMaxPerf();
  	}else{
  		for(let i in this.enjeux){
        // console.log(this.enjeux[i].key)
        // console.log(this.csvPhrases[this.curCsv].phrases[this.curPhrase].enjeu)
	  		if(this.enjeux[i].key == this.csvPhrases[this.curCsv].phrases[this.curPhrase].enjeu){
	  			console.log('defening enjeu %o',this.enjeux[i]);
	  			this.setEnjeu(this.enjeux[i]);
	  			this.perf = this.computePerf(this.csvPhrases[this.curCsv].phrases[this.curPhrase], this.enjeux[i]);
	  			break;
	  		}
	  		
	  	}
  		
  	}
  }

  getMaxPerf(){
    console.log("getting max perf");
  	let enjeu:Enjeu;
  	let perf = 0;
  	for(let i in this.enjeux){
  		this.setEnjeu(this.enjeux[i]);
  		break;
  	}
  	for(let i in this.enjeux){
  		enjeu = this.enjeux[i];
  		let cur = this.computePerf(this.csvPhrases[this.curCsv].phrases[this.curPhrase], this.enjeux[i]);
  		if(cur > perf){
  			console.log('cur %o', cur);
  			console.log('perf %o', perf);
  			console.log('enjeu %o', enjeu);
  			this.setEnjeu(this.enjeux[i]);
  		}
  	}



  }

  computePerf(phrase:Phrase, enjeu:Enjeu){
  	//mettre phrase en minuscule
  	let phrase_fr = phrase.phrase_fr.toLowerCase();
  	let phrase_en = phrase.phrase_en.toLowerCase();

  	// console.log(phrase_fr);
  	// console.log(phrase_fr);
  	// console.log("enjeu %o", enjeu);

  	let perf_fr = 0;
  	let perf_en = 0;
  	//fr
  	for(let i in enjeu.mots.fr){
  		if(phrase_fr.search(enjeu.mots.fr[i].mot.toLowerCase()) > 0){
  			perf_fr += enjeu.mots.fr[i].point;

  			console.log("found fr %o", enjeu.mots.fr[i].mot)
  		}
  	}

  	//en
  	for(let i in enjeu.mots.en){
  		if(phrase_en.search(enjeu.mots.en[i].mot.toLowerCase()) > 0){
  			perf_en += enjeu.mots.en[i].point;
  			console.log("found en %o", enjeu.mots.en[i].mot)
  		}
  	}

  	return (perf_fr > perf_en) ? perf_fr : perf_en;

  }

  public beforeChange($event: NgbPanelChangeEvent) {
  		console.log("panel id %o", $event.panelId);
  		console.log("cur csv  %o", this.curCsv);
  		this.firstLoading = false;
  		for(let i in this.csvPhrases){
	  			if(this.csvPhrases[i].nom == $event.panelId){
	  				this.curCsv = i;
	  				(document.getElementById(this.csvPhrases[i].nom+'-header').children[0].children[0] as HTMLElement).style.color = '#ffc107';
	  			}else{

	  				(document.getElementById(this.csvPhrases[i].nom+'-header').children[0].children[0] as HTMLElement).style.color = '#b0bec5';
	  			}
	  	}
  		let k = this.curCsv;
  		for(let j in this.csvPhrases[k].phrases){
  					if(!this.csvPhrases[k].phrases[j].valid){
  						console.log("non valid %o", this.csvPhrases[k].phrases[j]);
  						this.curPhrase = j;
  						break;
  					}
  					this.curPhrase = j;
  				}
  		this.getPerf();
      $event.preventDefault();
  }

}
