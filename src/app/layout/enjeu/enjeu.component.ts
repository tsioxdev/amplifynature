import { Component, OnInit } from '@angular/core';
import { EnjeuService } from '../../shared/services/enjeu.service';
import { TraductionService } from '../../shared/services/traduction.service';
import { Enjeu } from '../../shared/models/enjeu.model';

@Component({
  selector: 'app-enjeu',
  templateUrl: './enjeu.component.html',
  styleUrls: ['./enjeu.component.scss']
})
export class EnjeuComponent implements OnInit {

	enjeux:Enjeu[];
	enjeu:Enjeu;
	loading:Boolean = false;
	cur:string;

  traduction:any = {};

	constructor(
  	public enjeuService:EnjeuService,
    public tradService:TraductionService
  	) { }

  	ngOnInit() {
  		let self = this;
  		this.loading = true;

      this.tradService.getAll().on('value', function(data){
            data.forEach(function(child){
              self.traduction[child.val().ref] = child.val();
            });
            console.log("traduction %o", self.traduction);
      })

  		this.enjeuService.getAll().subscribe(function(data){
     
                
                self.loading = false;
                let enjeux = data.map(a => ({ key: a.key, ...a.payload.val() }));
                self.enjeux = enjeux.map((a:Enjeu) => new Enjeu().deserialize(a));
               	console.log("enjeux : %o", self.enjeux);
               	for(let i in self.enjeux){
               		self.setEnjeu(self.enjeux[i]);
               		break;
               	}
      })
  	}

  	setEnjeu(enjeu){
  		this.enjeu = new Enjeu().deserialize(enjeu);
  	}

    fr(mot){
      return this.traduction[mot] != undefined ? this.traduction[mot]['fr'] : mot;
    }

    en(mot){
      return this.traduction[mot] != undefined ? this.traduction[mot]['en'] : mot;
    }
}
