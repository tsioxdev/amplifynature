import { Injectable } from '@angular/core';
import { FileItem } from 'ng2-file-upload';

import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';
import { DropFileComponent } from '../../layout/drop-file/drop-file.component';
import { Papa } from 'ngx-papaparse';
import { Phrase } from "../models/phrase.model";
import { CsvPhrase } from "../models/csv-phrase.model";

@Injectable({
  providedIn: 'root'
})
export class PhraseService {

  constructor(
  	private db: AngularFireDatabase,
  	public papa:Papa,
  	private storage: AngularFireStorage

  	) { }

  	public export(item:FileItem, page:DropFileComponent){
  		let file = item['_file'];
  		let name = item.file.name;
  		this.papa.parse(file, {
  			header: true,
  			delimiter: ';',
  			beforeFirstChunk: function(chunk) {
                    let rows = chunk.split( /\r\n|\r|\n/ );
                    let headings = rows[0].toLowerCase().replace(/ /g, '');;
                    console.log("HEADING phrase : %o", headings);
                    rows[0] = headings;
                    return rows.join("\r\n");
                },
            complete: (result) => {
  				console.log("parsed phrase %o", result);
  				page.phraseLoading = true;
  				page.loading = true;

                let datas:any[] = this.check(result.data, name, page);
                if(datas.length > 0){
                	
                	let self = this;
                
                			
                			
                			
		                	self.add(self.convertHeader(datas)).then(function(){
		                		self.db.database.ref("csv_phrase").child(name.split('.')[0]).set({name: name}).then(function(){
							  		self.storage.ref('csv_phrase/'+name).put(file).then(function(storageSnap){
							  			storageSnap.ref.getDownloadURL().then(function(value){
							  				self.db.database.ref("csv_phrase").child(name.split('.')[0]+'/url').set(value).then(function(){
							  					page.loading = false;
									  			page.phraseLoading = false;

									  			page.success.push("Le fichier CSV Phrase "+name+ " a été exporté avec succès.");
							  				})
							  			})
							  			
							  		}).catch(function(error){
							  			console.log(error);
				                		page.errors.push(error);
				                	});
							  	}).catch(function(error){
							  		console.log(error);
				                		page.errors.push(error);
				                	});
		                	}).catch(function(error){
		                		console.log(error);
		                		page.errors.push(error);
		                	})
		                	
							  	
                
		                	
                }else{
                	page.loading = false;
                	console.log("Les données du fichier CSV Client ne sont pas conformes!");
                	// page.errors.push("Les données du fichier CSV Client ne sont pas conformes!");
                }

                page.phrase.clearQueue();
            }
        });
  	}

  	public list(){
	  	return this.db.database.ref('csv_phrase');
	  }

  	public add(csvPhrase:CsvPhrase){
  		console.log("adding csvPhrase %o", csvPhrase);
	  	return this.db.database.ref('phrases').child(csvPhrase.nom).set(csvPhrase);
	}

	public update(csvPhrase:CsvPhrase,phrase:Phrase,i){
  		console.log("adding csvPhrase %o", csvPhrase);
	  	this.db.database.ref('phrases/'+csvPhrase.nom+"/phrases/"+i+"/valid").set(phrase.valid);
	  	this.db.database.ref('phrases/'+csvPhrase.nom+"/phrases/"+i+"/enjeu").set(phrase.enjeu);
	  	this.db.database.ref('phrases/'+csvPhrase.nom+"/phrases/"+i+"/compteur_perf").set(phrase.compteur_perf);
	  	this.db.database.ref('phrases/'+csvPhrase.nom+"/phrases/"+i+"/satisfaction").set(phrase.satisfaction);
	}

	public getAll(){
		return this.db.list('csv_phrase').snapshotChanges();
	}

	public getWithPhrases(){
		return this.db.database.ref('phrases');
	}


	getCsv(name){
		return this.db.database.ref('phrases/'+name);
	}

	validate(phrase){
		return this.db.database.ref("csv_phrase/"+phrase.name+"/valid").set(true);
	}

	remove(item, page:DropFileComponent){
	    let self = this;
	    page.loading = true;
	    this.db.database.ref('csv_phrase/'+item.name.split('.')[0]).remove().then(function(){
	    	self.db.database.ref('phrases/'+item.name.split('.')[0]).remove().then(function(){
	    		self.storage.ref('csv_phrase/'+item.name).delete().subscribe(function(){
			           page.loading = false;
			           page.success.push('Csv Phrase '+ item.name+' supprimé avec succès.');
			    });
	    	})
			      
	    })
	}

	convertHeader(datas){
		let res:CsvPhrase = new CsvPhrase();
		let phrases:Phrase[] = [];
  		for (let i = 0; i < datas.length; ++i) {
  			let csvPhrase = datas[i];

  				let phrase = new Phrase();
  				phrase.num = this.getCol(csvPhrase, "num_phrase");
  				phrase.langue = this.getCol(csvPhrase, "langue_phrase");
  				phrase.compteur_perf = this.getCol(csvPhrase, "compteur_perf");
  				phrase.enjeu = this.getCol(csvPhrase, "enjeu_a_valider");
  				phrase.phrase_fr = this.getCol(csvPhrase, "phrase_fr");
  				phrase.phrase_en = this.getCol(csvPhrase, "phrase_en");
  				phrase.flux = this.getCol(csvPhrase, "flux_appartenance");

  				phrases.push(phrase);
  			if(i ==0 ){		
  				res.nom = this.getCol(csvPhrase, "nom_csv");
			  	res.nom_pdf = this.getCol(csvPhrase, "nom_pdf");
			  	res.titre_fr = this.getCol(csvPhrase, "titre_fr");
			  	res.titre_en = this.getCol(csvPhrase, "titre_en");
			  	res.auteur = this.getCol(csvPhrase, "auteur");
			  	res.date = this.getCol(csvPhrase, "date");
			  	res.pays = this.getCol(csvPhrase, "pays");
			  	res.long = this.getCol(csvPhrase, "coord_gps_x");
			  	res.lat = this.getCol(csvPhrase, "coord_gps_y");
  			}
  		}

  		res.phrases = phrases;
			  

	  	return res;
	}

	public getCol(csvPhrase:any, col:string){
	  	return csvPhrase[col] ? csvPhrase[col] : "";
	 }

  	check(datas, nom_csv:string, page:DropFileComponent){
  		let length = 0;
	  	let res = [];

	  	

	  	let checkArray = ['langue_phrase', 'compteur_perf','enjeu_a_valider', 'satisfaction_a_valider', 'nom_csv', 'nom_pdf', 'phrase_fr', 'phrase_en', 'flux_appartenance', 'titre_fr', 'titre_en', 'auteur', 'date', 'pays', 'coord_gps_x', 'coord_gps_x'];

	  		
	  	for (var i = 0; i < datas.length; ++i) {

	  		let ok =true;

	  		if(datas[i]['num_phrase'] == undefined || datas[i]['num_phrase'] == ""){

	  			
	  				page.errors.push("Erreur lors de l'exportation du fichier CSV Phrase : Ligne "+Number(i+2)+" <br> "
	  					+JSON.stringify(datas[i])+"<br> <strong> Format en-tête à respecter : </strong> num_phrase;langue_phrase;compteur_perf;enjeu_a_valider;satisfaction_a_valider;nom_CSV;nom_PDF;phrase_FR;phrase_EN;flux_appartenance;titre_FR;titre_EN;auteur;date;pays;coord_gps_x;coord_gps_y <br> <strong> La colonne nom_csv est le nom du fichier </strong>"
	  					);

		  			page.errors.push("Erreur. Colonne <strong> num_phrase </strong> : "+datas[i]['num_phrase']);

		  			
		  			ok = false;
	  
	  		}
	  		for(let j in checkArray){
	  			if(datas[i][checkArray[j]] == undefined  ){
	  			
	  				page.errors.push("Erreur lors de l'exportation du fichier CSV Phrase : Ligne "+Number(i+2)+" <br> "
	  					+JSON.stringify(datas[i])+"<br> <strong> Format en-tête à respecter : </strong> num_phrase;langue_phrase;compteur_perf;enjeu_a_valider;satisfaction_a_valider;nom_CSV;nom_PDF;phrase_FR;phrase_EN;flux_appartenance;titre_FR;titre_EN;auteur;date;pays;coord_gps_x;coord_gps_y <br> <strong> La colonne nom_csv est le nom du fichier </strong>"
	  					);

		  			page.errors.push("Erreur. Colonne <strong> "+checkArray[j]+"</strong> : "+datas[i][checkArray[j]]);

		  			
		  			ok = false;
	  			}
	  		}
	  		// if(datas[i]['num_phrase'] == undefined || datas[i]['num_phrase'] == "" ||
	  		// 	datas[i]['langue_phrase'] == undefined ||
	  			

	  		// 	datas[i]['compteur_perf'] == undefined ||
	  		// 	datas[i]['enjeu_a_valider'] == undefined ||
	  		// 	datas[i]['satisfaction_a_valider'] == undefined ||

	  		// 	datas[i]['nom_csv'] == undefined ||
	  		// 	datas[i]['nom_pdf'] == undefined ||

	  		// 	datas[i]['phrase_fr'] == undefined ||
	  		// 	datas[i]['phrase_en'] == undefined ||

	  		// 	datas[i]['flux_appartenance'] == undefined ||
	  		// 	datas[i]['titre_fr'] == undefined ||
	  		// 	datas[i]['titre_en'] == undefined ||
	  		// 	datas[i]['auteur'] == undefined ||
	  		// 	datas[i]['date'] == undefined ||
	  		// 	datas[i]['pays'] == undefined ||
	  		// 	datas[i]['coord_gps_x'] == undefined ||
	  		// 	datas[i]['coord_gps_y'] == undefined 
	  			
	  		// 	){
	  			

	  		// 	if(res.length == 0){
	  				
	  		// 		page.errors.push("Erreur lors de l'exportation du fichier CSV Phrase : Ligne "+Number(i+2)+" <br> "+JSON.stringify(datas[i])+"<br> <strong> Format en-tête à respecter : </strong> num_phrase;langue_phrase;compteur_perf;enjeu_a_valider;satisfaction_a_valider;nom_CSV;nom_PDF;phrase_FR;phrase_EN;flux_appartenance;titre_FR;titre_EN;auteur;date;pays;coord_gps_x;coord_gps_y <br> <strong> La colonne nom_csv est le nom du fichier </strong> ");
	  		// 		break;
	  		// 	}else{
	  		// 		page.errors.push("Fichier CSV Phrase : La ligne "+Number(i+2)+" ne peut pas être exportée <br> "+JSON.stringify(datas[i])+"<br> <strong> La colonne nom_csv est le nom du fichier </strong>");
	  		// 	}
	  		// 	// console.log("error in %o", datas[i]);
	  			
	  		// }else{
	  		// 	if(datas[i]['nom_csv'] !== nom_csv.split('.')[0] && res.length == 0){
	  		// 		page.errors.push("Erreur lors de l'exportation du fichier CSV Phrase : Ligne "+Number(i+2)+" <br> "+JSON.stringify(datas[i])+"<br> <strong> Format en-tête à respecter : </strong> num_phrase;langue_phrase;compteur_perf;enjeu_a_valider;satisfaction_a_valider;nom_CSV;nom_PDF;phrase_FR;phrase_EN;flux_appartenance;titre_FR;titre_EN;auteur;date;pays;coord_gps_x;coord_gps_y <br> <strong> La colonne nom_csv est le nom du fichier </strong> ");
	  		// 		break;
	  		// 	}else{
	  		// 		res.push(datas[i]);
	  		// 	}
	  			
	  		// }

	  		if(!ok){
	  			break;
	  		}
			if(ok){
	  			res.push(datas[i]);
	  		}
	  	}
	  	return res;
  	}
}
