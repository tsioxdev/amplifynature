import { TestBed } from '@angular/core/testing';

import { OrgaPngService } from './orga-png.service';

describe('OrgaPngService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OrgaPngService = TestBed.get(OrgaPngService);
    expect(service).toBeTruthy();
  });
});
