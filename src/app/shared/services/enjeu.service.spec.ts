import { TestBed } from '@angular/core/testing';

import { EnjeuService } from './enjeu.service';

describe('EnjeuService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EnjeuService = TestBed.get(EnjeuService);
    expect(service).toBeTruthy();
  });
});
