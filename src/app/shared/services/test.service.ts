import { Injectable } from '@angular/core';

import { AngularFireDatabase } from '@angular/fire/database';
import { Client } from "../models/client.model";

// import { Papa } from 'ngx-papaparse';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(private db: AngularFireDatabase,
  	// public papa:Papa
  	) { }

  public add(client:Client){
  	return this.db.database.ref('clients').child(client.num).set(client);
  }

  public list(){
  	return this.db.list("clients").snapshotChanges();
  }

  public remove(client){
  	return this.db.database.ref('clients/'+client.num).remove();
  }

  // public export(file:File){
  // 	this.papa.parse(file, {
  //           complete: (result) => {
  //               console.log('Parsed: ', result);
  //           }
  //       });
  // }

  
  public test(){
  	console.log("client service test");
  }

}
