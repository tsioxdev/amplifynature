import { Injectable } from '@angular/core';

import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';
import { DropFileComponent } from '../../layout/drop-file/drop-file.component';
import { Papa } from 'ngx-papaparse';
import { Couleur } from "../models/couleur.model";

@Injectable({
  providedIn: 'root'
})
export class CouleurService {

  constructor(
  	private db: AngularFireDatabase,
  	public papa:Papa,
  	private storage: AngularFireStorage

  	) { }

  	public export(file:File, name:string, page:DropFileComponent){
  		this.papa.parse(file, {
  			header: true,
  			beforeFirstChunk: function(chunk) {
                    let rows = chunk.split( /\r\n|\r|\n/ );
                    let headings = rows[0].toLowerCase();
                    console.log("HEADING couleur : %o", headings);
                    rows[0] = headings;
                    return rows.join("\r\n");
                },
            complete: (result) => {
  				console.log("parsed couleur %o", result)
                let datas:any[] = this.check(result.data, page);
                if(datas.length > 0){
                	page.loading = true;
			        page.couleurLoading = true;
                	let self = this;
                	this.db.database.ref('couleur').remove().then(function(){
                			for (let i = 0; i < datas.length; ++i) {
		                		self.add(self.convertHeader(datas[i]));
		                	}
						  	self.db.database.ref("csv_couleur").set({name: name}).then(function(){
      						  		self.storage.ref('csv_couleur/'+name).put(file).then(function(snapshot){
			                        snapshot.ref.getDownloadURL().then(function(value){
			                              self.db.database.ref("csv_couleur/url").set(value).then(function(){
			                                  page.loading = false;
			                                  page.couleurLoading = false;
			                                  page.success.push("Le fichier CSV Couleur "+name+ " a été uploadé avec succès.");
			                              })
			                        })
                              
      						  			
      						  			
      						  	});
      						})
                	});
		                	
                }else{
                	page.loading = false;
                	page.couleurLoading = false;
                	console.log("Les données du fichier CSV Client ne sont pas conformes!");
                	// page.errors.push("Les données du fichier CSV Client ne sont pas conformes!");
                }

                page.couleur.clearQueue();
            }
        });
  	}

  	public getfile(){
	  	return this.db.object('csv_couleur').snapshotChanges();
	  }

	public remove(csvEnjeu, page:DropFileComponent){
	    let self = this;
	    page.loading = true;
	  	this.db.database.ref('csv_couleur/name').remove().then(function(){
	      self.db.database.ref('csv_couleur').remove().then(function(){
	          self.db.database.ref('couleur').remove().then(function(){
	            self.storage.ref('csv_couleur/'+csvEnjeu.name).delete().subscribe(function(){
	              page.loading = false;
	              page.success.push("Fichier csv couleur supprimé avec succès.");
	            })
	          })
	      })
	          
	    });
	}

  	public add(couleur:Couleur){
  		console.log("adding couleur %o", couleur);
	  	return this.db.database.ref('couleur').child(couleur.objet.replace(/#/g, '')).set(couleur);
	}

	convertHeader(trad){
		let res:Couleur = new Couleur();
  	
	  	res.objet = this.getCol(trad, "objet_front_back_office");
	  	res.couleur = this.getCol(trad, "ref_couleur");
	  	return res;
	}

	public getCol(trad:any, col:string){
	  	return trad[col] ? trad[col] : "";
	 }

  	check(datas, page:DropFileComponent){
  		let length = 0;
	  	let res = [];
	  	for (var i = 0; i < datas.length; ++i) {
	  		let ok = true;
	  		if(!datas[i]['objet_front_back_office'] || datas[i]['objet_front_back_office'] == ""){
	  			ok = false;
	  			page.errors.push("Erreur lors de l'exportation du fichier CSV Couleur : Ligne "+Number(i+2)+" <br> "+JSON.stringify(datas[i])+"<br> <strong> Format en-tête à respecter : </strong> objet_front_back_office;ref_couleur");
	  			page.errors.push("Erreur.Colonne <strong> objet_front_back_office</strong> :"+datas[i]['objet_front_back_office']);
	  		}

	  		if(!datas[i]['ref_couleur']){
	  			ok = false;
	  			page.errors.push("Erreur lors de l'exportation du fichier CSV Couleur : Ligne "+Number(i+2)+" <br> "+JSON.stringify(datas[i])+"<br> <strong> Format en-tête à respecter : </strong> objet_front_back_office;ref_couleur");
	  			page.errors.push("Erreur.Colonne <strong> ref_couleur</strong> :"+datas[i]['ref_couleur']);
	  		}
	  		if(!ok){
	  			break;
	  		}else{
	  			res.push(datas[i]);
	  		}

	  		// if(!datas[i]['objet_front_back_office'] || datas[i]['objet_front_back_office'] == "" ||
	  		// 	!datas[i]['ref_couleur']
	  		// 	){
	
	  		// 	if(res.length == 0){
	  		// 		page.errors.push("Erreur lors de l'exportation du fichier CSV Couleur : Ligne "+Number(i+2)+" <br> "+JSON.stringify(datas[i])+"<br> <strong> Format en-tête à respecter : </strong> objet_front_back_office;ref_couleur");
	  		// 		break;
	  		// 	}else{
	  		// 		page.errors.push("Fichier CSV Couleur : La ligne "+Number(i+2)+" ne peut pas être exportée <br> "+JSON.stringify(datas[i]));
	  		// 	}
	  		// 	// console.log("error in %o", datas[i]);
	  			
	  		// }else{
	  		// 	res.push(datas[i]);
	  		// }
	  	}
	  	return res;
  	}
}
