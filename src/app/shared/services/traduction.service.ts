import { Injectable } from '@angular/core';

import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';
import { DropFileComponent } from '../../layout/drop-file/drop-file.component';
import { Papa } from 'ngx-papaparse';
import { Traduction } from "../models/traduction.model";

@Injectable({
  providedIn: 'root'
})
export class TraductionService {

  constructor(
  	private db: AngularFireDatabase,
  	public papa:Papa,
  	private storage: AngularFireStorage

  	) { }

  	public export(file:File, name:string, page:DropFileComponent){
  		this.papa.parse(file, {
  			header: true,
  			beforeFirstChunk: function(chunk) {
                    let rows = chunk.split( /\r\n|\r|\n/ );
                    let headings = rows[0].toLowerCase().replace(/ /g, '');;
                    console.log("HEADING phrase : %o", headings);
                    rows[0] = headings;
                    return rows.join("\r\n");
                },
            complete: (result) => {
  				console.log("pqrsed %o", result)
                let datas:any[] = this.check(result.data, page);
                if(datas.length > 0){
                	
                	let self = this;
                	page.loading = true;
			        page.tradLoading = true;
                	this.db.database.ref('traduction').remove().then(function(){
                			for (let i = 0; i < datas.length; ++i) {
		                		self.add(self.convertHeader(datas[i]));
		                	}
						  		self.db.database.ref("csv_traduction").set({name: name}).then(function(){
      						  		self.storage.ref('csv_traduction/'+name).put(file).then(function(snapshot){
			                          snapshot.ref.getDownloadURL().then(function(value){
			                              self.db.database.ref("csv_traduction/url").set(value).then(function(){
			                                  page.loading = false;
			                                  page.tradLoading = false;
			                                  page.success.push("Le fichier CSV Traduction "+name+ " a été uploadé avec succès.");
			                              })
			                          })
                              
      						  			
      						  			
      						  		});
      						  	})
                	});
		                	
                }else{
                	page.loading = false;
                	page.tradLoading = false;
                	console.log("Les données du fichier CSV Client ne sont pas conformes!");
                	// page.errors.push("Les données du fichier CSV Client ne sont pas conformes!");
                }

                page.traduction.clearQueue();
            }
        });
  	}

  	public getfile(){
	  	return this.db.object('csv_traduction').snapshotChanges();
	  }

	public getAll(){
	  	return this.db.database.ref('trad');
	}

	public remove(csvTrad, page:DropFileComponent){
	    let self = this;
	    page.loading = true;
	  	this.db.database.ref('csv_traduction/name').remove().then(function(){
	      self.db.database.ref('csv_traduction').remove().then(function(){
	          self.db.database.ref('trad').remove().then(function(){
	            self.storage.ref('csv_traduction/'+csvTrad.name).delete().subscribe(function(){
	              page.loading = false;
	              page.success.push("Fichier csv client supprimé avec succès.");
	            })
	          })
	      })
	          
	    });
	  }
  	public add(trad:Traduction){
  		console.log("addinfg %o", trad);
	  	return this.db.database.ref('trad').child(trad.ref.replace(/#/g, '')).set(trad);
	}

	convertHeader(trad){
		let res:Traduction = new Traduction();
  	
	  	res.ref = this.getCol(trad, "ref_traduction");
	  	// .replace(/#/g, '');
	  	res.fr = this.getCol(trad, "fr");
	  	res.en = this.getCol(trad, "en");


	  	return res;
	}

	public getCol(trad:any, col:string){
	  	return trad[col] ? trad[col] : "";
	 }

  	check(datas, page:DropFileComponent){
  		let length = 0;
	  	let res = [];
	  	for (var i = 0; i < datas.length; ++i) {
	  		let ok = true;
	  		if(!datas[i]['ref_traduction'] || datas[i]['ref_traduction'] == ""){
	  			ok = false;
	  			page.errors.push("Erreur lors de l'exportation du fichier CSV Traduction : Ligne "+Number(i+2)+" <br> "+JSON.stringify(datas[i])+"<br> <strong> Format en-tête à respecter : </strong> ref_traduction;fr;en");
	  			page.errors.push("Erreur.Colonne <strong> ref_traduction</strong> :"+datas[i]['ref_traduction']);
	  		}

	  		if(!datas[i]['fr']){
	  			ok = false;
	  			page.errors.push("Erreur lors de l'exportation du fichier CSV Traduction : Ligne "+Number(i+2)+" <br> "+JSON.stringify(datas[i])+"<br> <strong> Format en-tête à respecter : </strong> ref_traduction;fr;en");
	  			page.errors.push("Erreur. Colonne <strong> fr</strong> :"+datas[i]['fr']);
	  		}

	  		if(!datas[i]['en']){
	  			ok = false;
	  			page.errors.push("Erreur lors de l'exportation du fichier CSV Traduction : Ligne "+Number(i+2)+" <br> "+JSON.stringify(datas[i])+"<br> <strong> Format en-tête à respecter : </strong> ref_traduction;fr;en");
	  			page.errors.push("Erreur. Colonne <strong> en</strong> :"+datas[i]['en']);
	  		}

	  		if(!ok ){
	  			break;
	  		}else{
	  			res.push(datas[i]);
	  		}
	  		// if(!datas[i]['ref_traduction'] || datas[i]['ref_traduction'] == "" ||
	  		// 	!datas[i]['fr']
	  		// 	){
	  	
	  		// 	if(res.length == 0){
	  		// 		page.errors.push("Erreur lors de l'exportation du fichier CSV Traduction : Ligne "+Number(i+2)+" <br> "+JSON.stringify(datas[i])+"<br> <strong> Format en-tête à respecter : </strong> ref_traduction;fr;en");
	  		// 		break;
	  		// 	}else{
	  		// 		page.errors.push("Fichier CSV Traduction : La ligne "+Number(i+2)+" ne peut pas être exportée <br> "+JSON.stringify(datas[i]));
	  		// 	}
	  		// 	// console.log("error in %o", datas[i]);
	  			
	  		// }else{
	  		// 	res.push(datas[i]);
	  		// }
	  	}
	  	return res;
  	}
}
