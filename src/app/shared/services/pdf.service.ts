import { Injectable } from '@angular/core';
import { FileItem } from 'ng2-file-upload';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireDatabase } from '@angular/fire/database';

import { DropFileComponent } from '../../layout/drop-file/drop-file.component';

@Injectable({
  providedIn: 'root'
})
export class PdfService {

  constructor(
  	private storage:AngularFireStorage,
  	private db:AngularFireDatabase
  	) { }


  export(item:FileItem, page:DropFileComponent){
  	console.log("exporting %o", item);
  	let self = this;
    let name = item.file.name;
    let file = item['_file'];
  	this.db.database.ref('phrases').orderByChild("nom_pdf").equalTo(name.split('.')[0]).once('value', function(snapshot){
  		if(snapshot.val()){
  			snapshot.forEach(function(childSnap){
  				page.loading = true;
  				page.pdfLoading = true;
  				self.storage.ref("pdf/"+name).put(file).then(function(storageSnap){
  					storageSnap.ref.getDownloadURL().then(value =>{
			            console.log(value);
			            self.db.database.ref('phrases/'+childSnap.val().nom+"/pdf_url/").set(value).then(function(){
			            	page.loading = false;
			            	page.pdfLoading = false;
			            	page.success.push("PDF "+name+" uploadé avec succès.");
			            	self.db.database.ref('pdf').child(name.split('.')[0]).set({'name': name, 'url': value}).then(function(){
			            
			            		
			            	
			            		
			            	}).catch(function(error){
			            		page.errors.push(error);
			            	});
			            	
			            })
			       
			        });
  				}).catch(function(error){
			        page.errors.push(error);
			    });
  			});

  		}else{
  			page.enjeuPngLoading = false;
  			page.loading = false;
  			page.errors.push("Aucune référence ne correspond au pdf :"+name);
  		}
      page.pdf.removeFromQueue(item);
  	})
  }
 

  list(){
  	return this.db.list('pdf').snapshotChanges();
  }

  remove(item, page:DropFileComponent){
    let self = this;
    page.loading = true;
    this.db.database.ref('pdf/'+item.name.split('.')[0]).remove().then(function(){
    	 page.loading = true;
      self.storage.ref('pdf/'+item.name).delete().subscribe(function(){
           page.loading = false;
           page.success.push('PDF '+ item.name+' supprimé avec succès.');
      });
    })
  }
}
