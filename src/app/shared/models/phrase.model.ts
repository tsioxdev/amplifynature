
import { Deserializable } from "./deserializable.model";
import { Enjeu } from "./enjeu.model";

export class Phrase implements Deserializable{

	num:string;
	langue:string;
	compteur_perf:string;
	enjeu:any = 0;
	satisfaction:number = 0;
	phrase_en:string;
	phrase_fr:string;
	flux:string;
	valid:boolean = false;

	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}

