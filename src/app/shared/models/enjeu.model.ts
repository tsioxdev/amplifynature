
import { Deserializable } from "./deserializable.model";
import { MotEnjeu } from "./motEnjeu.model";

export class LangEnjeu{
	en:MotEnjeu[];
	fr:MotEnjeu[];
}
export class Enjeu implements Deserializable{

	key:string;
	num:string;
	titre:string;
	pre_def:string;
	def:string;
	image_url:string;
	picto_png:string;
	mots:LangEnjeu;

	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}
