
import { Deserializable } from "./deserializable.model";

export class Couleur implements Deserializable{

	objet:string;
	couleur:string;

	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}

