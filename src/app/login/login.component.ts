import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { routerTransition } from '../router.animations';

import { AngularFireAuth } from '@angular/fire/auth';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    email: string;
    password: string;
    error: string= "";
    constructor(
        private translate: TranslateService,
        public router: Router,
        private auth: AngularFireAuth

        ) {
            this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
            this.translate.setDefaultLang('en');
            const browserLang = this.translate.getBrowserLang();
            this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');
    }

    ngOnInit() {}

    onLoggedin() {
        let self = this;
        console.log(this.email );
        console.log(this.password );
        if(this.email && this.password){
            this.auth.auth.signInWithEmailAndPassword(this.email, this.password).then(function(){
                self.error = "";
                localStorage.setItem('isLoggedin', 'true');
                self.router.navigate(['/drop-file']);

            }).catch(function(error) {
                  self.error = error;
            });
        }
    }
}
